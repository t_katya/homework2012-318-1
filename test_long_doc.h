﻿#ifndef TEST_LONG_H
#define TEST_LONG_H

#include <cppunit/extensions/HelperMacros.h>
#include"Long_doc.h"

using namespace CppUnit;

/**
* В этом классе описанны тесты для объектов класса Long.
* <p> Класс Test_long содержит тесты для операций над объектами класса Long.
* Является наследником TestFixture
* @author Taratuta Yekaterina
*/

class Test_Long : public TestFixture {
      
    CPPUNIT_TEST_SUITE(Test_Long);
	CPPUNIT_TEST(ADD);
	CPPUNIT_TEST(SUB);
	CPPUNIT_TEST(MUL);
	CPPUNIT_TEST(DIV);
	CPPUNIT_TEST(LESS);
	CPPUNIT_TEST(MORE);
	CPPUNIT_TEST(LESSEQUAL);
	CPPUNIT_TEST(MOREEQUAL);
	CPPUNIT_TEST(EQUAL);
	CPPUNIT_TEST(NOTEQUAL);
	CPPUNIT_TEST_SUITE_END();

  public:
         /**
         * <tt>setUp()</tt> - выделение памяти для объектов класса Test_long
         */
      void setUp();
      /**
         * <tt>tearDown()</tt> - очищение памяти для объектов класса Test_long
         */
	  void tearDown();	
	  /**
         * <tt>ADD()</tt> - проверка на сложение для объектов класса Test_long
         */
	  void ADD();
	  /**
         * <tt>SUB()</tt> - проверка на разность памяти для объектов класса Test_long
         */
	  void SUB();
	  /**
         * <tt>MUL()</tt> - проверка на произведение для объектов класса Test_long
         */
	  void MUL();
	  /**
         * <tt>DIV()</tt> - проверка на деление для объектов класса Test_long
         */
	  void DIV();
	  /**
         * <tt>LESS</tt> - проверка на операцию меньше для объектов класса Test_long
         */
	  void LESS();
	  /**
         * <tt>MORE()</tt> - проверка на операцию больше для объектов класса Test_long
         */
	  void MORE();
	  /**
         * <tt>LESSEQUAL()</tt> - проверка на операцию меньше-равно для объектов класса Test_long
         */
	  void LESSEQUAL();
	  /**
         * <tt>MOREEQUAL()</tt> - проверка на операцию больше-равно для объектов класса Test_long
         */
	  void MOREEQUAL();
	  /**
         * <tt>EQUAL()</tt> - проверка на равенство для объектов класса Test_long
         */
	  void EQUAL();
	  /**
         * <tt>NOTEQUAL()</tt> - проверка на неравенство для объектов класса Test_long
         */
	  void NOTEQUAL();
 
  private:
	  Long *A, *B, *C;

};

#endif
