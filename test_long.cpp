#include"test_long.h"
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

using namespace CppUnit;

CPPUNIT_TEST_SUITE_REGISTRATION(Test_Long);

void Test_Long::setUp() {
	
    A = new Long("1");
	B = new Long("11");
	C = new Long("111");
}

void Test_Long::tearDown() {
	
    delete A;
	delete B;
	delete C;
}

void Test_Long::ADD() {
	
    *A = Long("1234567890123456789");
	*B = Long("1597530000008246");
	*C = Long("1236165420123465035");
	CPPUNIT_ASSERT( *A + *B == *C );
    *A = Long("-2584567319");
	*B = Long("12545213564798");
	*C = Long("12542628997479");
	CPPUNIT_ASSERT( *A + *B == *C );
    *A = Long("-197328465");
	*B = Long("-123456789");
	*C = Long("-73871676");
	CPPUNIT_ASSERT( *A + *B == *C );
}

void Test_Long::SUB(){

	*A = Long("7412589633214");
	*B = Long("741258");
	*C = Long("7412588891956");
	CPPUNIT_ASSERT( *A - *B == *C );
    *A = Long("-147852369");
	*B = Long("19735");
	*C = Long("-147872104");
	CPPUNIT_ASSERT( *A - *B == *C );
    *A = Long("-123456789");
	*B = Long("-82465");
	*C = Long("-123374324");
	CPPUNIT_ASSERT( *A - *B == *C );
    *A = Long("741258963");
	*B = Long("-14750");
	*C = Long("741273713");
	CPPUNIT_ASSERT( *A - *B == *C );
}

void Test_Long::MUL() {
	
    *A = Long("1254789523654");
	*B = Long("4255");
	*C = Long("5339129423147770");
	CPPUNIT_ASSERT( ( *A ) * ( *B ) == *C );
    *A = Long("145236578");
	*B = Long("-12547");
	*C = Long("-1822283344166");
	CPPUNIT_ASSERT( ( *A ) * ( *B ) == *C );
	*A = Long("-14523");
	*B = Long("-74125895175346825");
	*C = Long("1076530375631561939475");
	CPPUNIT_ASSERT( ( *A ) * ( *B ) == *C );
}

void Test_Long::DIV() {
     
	*A = Long("44444444444444444444");
	*B = Long("2");
	*C = Long("22222222222222222222");
    CPPUNIT_ASSERT( *A / *B == *C );
    *A = Long("-74125698745236");
	*B = Long("4232");
	*C = Long("-17515524278");
    CPPUNIT_ASSERT( *A / *B == *C );
    *A = Long("123456789");
	*B = Long("-147");
	*C = Long("-839842");
    CPPUNIT_ASSERT( *A / *B == *C );
    *A = Long ("-74125896355555");
	*B = Long ("-14785");
	*C = Long ("5013587849");
    CPPUNIT_ASSERT( *A / *B == *C );
    *A = Long ("741");
	*B = Long ("14785");
	*C = Long ("0");
    CPPUNIT_ASSERT( *A / *B == *C );
    *A = Long ("741");
	*B = Long ("741");
	*C = Long ("1");
    CPPUNIT_ASSERT( *A / *B == *C );
}



void Test_Long::LESS() {
     
	*A = Long("9641235478965412");
	*B = Long("99954123654782524");
	CPPUNIT_ASSERT( *A < *B );
	*A = Long("-41254781");
	*B = Long("541236547");
	CPPUNIT_ASSERT( *A < *B );
	*A = Long("-4125478");
	*B = Long("-41256");
	CPPUNIT_ASSERT( *A < *B );
}

void Test_Long::MORE() {
	
    *A = Long ("9999990519");
	*B = Long ("985423948");
	CPPUNIT_ASSERT( *A > *B );
    *A = Long ("785");
	*B = Long ("-741256336987554");
	CPPUNIT_ASSERT( *A > *B );
	*A = Long ("-7452785");
	*B = Long ("-956987554");
	CPPUNIT_ASSERT( *A > *B );
}

void Test_Long::LESSEQUAL() {
     
	*A = Long("57438989");
	*B = Long("91543543538");
	CPPUNIT_ASSERT( *A <= *B );
    *A = Long("123456789");
	*B = Long("123456789");
	CPPUNIT_ASSERT( *A <= *B );
    *A = Long("-99999997593475");
	*B = Long("-5789347598");
	CPPUNIT_ASSERT( *A <= *B );
}

void Test_Long::MOREEQUAL() {
	
    *A = Long ("7548758475893475897348534534957");
	*B = Long ("51315645115121");
	CPPUNIT_ASSERT( *A >= *B );	
	*A = Long ("123456789");
	*B = Long ("123456789");
	CPPUNIT_ASSERT( *A >= *B );
	*A = Long ("-7412599655");
	*B = Long ("-999944125522332225514541");
	CPPUNIT_ASSERT( *A >= *B );
}

void Test_Long::NOTEQUAL() {
	
    *A = Long ("757567465746235764327856344957");
	*B = Long ("51315645115121");
	CPPUNIT_ASSERT( *A != *B );
	*A = Long ("-123456789");
	*B = Long ("123456789");
	CPPUNIT_ASSERT( *A != *B );
	*A = Long ("-7412599655");
	*B = Long ("-999944125522332225514541");
	CPPUNIT_ASSERT( *A != *B );
}

void Test_Long::EQUAL() {
	
    *A = Long ("123456789");
	*B = Long ("123456789");
	CPPUNIT_ASSERT( *A == *B );
	*A = Long ("-9876543210");
	*B = Long ("-9876543210");
	CPPUNIT_ASSERT( *A == *B );
}


int main( int argc, char **argv) {
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest( registry.makeTest() );
    runner.run();
return 0;
}

