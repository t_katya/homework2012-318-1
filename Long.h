#ifndef LONG_ARR_H
#define LONG_ARR_H

#include <iostream>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>

using namespace std;
//------------------------------------------------------------------------------

class Long {
    
    int *m;
    int *bin;
    int len;
    int binlen;
    int flg;
    
  public:
    Long ();
    Long ( string );
    Long& operator= ( const Long& );
    friend Long Modul ( Long );
    friend Long operator+ ( const Long&, const Long & );
	friend Long operator- ( const Long&, const Long& );
	friend Long operator* ( const Long&, const Long& );
	friend Long operator/ ( const Long&, const Long& );
	friend Long operator% ( const Long&, const Long& );
	friend bool operator> ( const Long&, const Long& );
	friend bool operator< ( const Long&, const Long& );
	friend bool operator<= ( const Long&, const Long& );
	friend bool operator>= ( const Long&, const Long& );
	friend bool operator== ( const Long&, const Long& );
	friend bool operator!= ( const Long&, const Long& );
	friend Long & operator+= ( Long&, const Long& );
	friend Long & operator-= ( Long&, const Long& );
	friend Long & operator*= ( Long&, const Long& );
	friend Long & operator/= ( Long& , const Long& );
	friend ostream& operator<< ( ostream &, const Long& );
	friend istream& operator>> ( istream &,  Long& );
          
};

int numbers ( int A );
bool Compare ( int *C1, int *C2, int csize );
bool Test( const char * pSting, char ch );
void turnstring ( int *s, int size );
int *plusbin ( int *U, int *W, int su, int sw, int &Slen );
int *multiplybin( int *U, int *W, int su, int sw, int &Pr );
int *minusbin ( int *U, int *W, int su, int sw, int &Mlen );
int *divisionbin ( int *U, int *W, int su, int sw, int &Dlen );
int *BinToDec ( int *Sbin, int Slen, int *Sdec, int &Shlen );
int *DecToBin ( int razmer, int *X, int *w, int &s1 );


#endif
