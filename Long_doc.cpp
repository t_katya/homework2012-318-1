#include"Long_doc.h"
#include"exception_doc.h"

using namespace std;

//------------------------------------------------------------------------------
int numbers ( int A ) {

    int num = 0;
    
    if( A == 0 ) num = 1;
    else {    
        while ( A != 0 ) {
            num = num + 1;
            A = A / 10;
        }
    }
    return num;
}
//------------------------------------------------------------------------------
bool Compare ( int *mas1, int *mas2, int massize ) {
    
    int i;
    
    for ( i = 0; i < massize; i++ ) {
        if ( ( mas1[i] == 1 ) && ( mas2[i]==0 ) ) return true;
        if ( ( mas1[i] == 0 ) && ( mas2[i]==1 ) ) return false;
    }
    return true; 
}
//------------------------------------------------------------------------------
bool Test( const char * pSting, char ch ) { 
  
    if ( pSting ) { 
        while ( *pSting ) {
           if ( *pSting == ch ) return true;
            pSting++; 
            } 
    } 
    return false; 
}
//------------------------------------------------------------------------------
void turnstring ( int *s, int size ) {
 
    int c;
    int i;
    for ( i = 0; i <= ( size - 1 ) / 2; i++ ) {
        c = s[i]; 
        s[i] = s[size - i - 1]; 
        s[size - i - 1] = c;
    }      
}
//------------------------------------------------------------------------------
int *plusbin ( int *U, int *W, int sizeu, int sizew ,int &Sumlen ) {
    
    int ost = 0;
    int i = 0, k;
    int *Sum = NULL;
    
    while ( i != sizew ) {
        Sum = ( int * ) realloc ( Sum, ( ++i )*sizeof( int ) );
        if ( ( U[i-1] == 0 ) && ( W[i-1] == 0 ) ) {
            Sum[i-1] = ost;
            ost = 0;
        }
        if ( ( ( U[i-1] == 0 ) && ( W[i-1] == 1 ) ) || ( ( U[i-1] == 1 ) && ( W[i-1] == 0 ) ) ) {      
            if( ost == 0 ) Sum[i-1] = 1;
            else {
                Sum[i-1] = 0;               
                ost = 1;
            }
        }
        else if( ( U[i-1] == 1 ) && ( W[i-1] == 1 ) ) {
            if ( ost == 0 ) {
                  Sum[i-1] = 0;
                  ost = 1;
            } else {
                Sum[i-1] = 1;
                ost = 1;
            }
        }
    }
    k = i;
    while ( k != sizeu ) {          
        Sum = ( int * ) realloc ( Sum, ( ++k )*sizeof( int ) );
        if ( k > 1000000000 ) {
            repletion exp;
            throw exp;        
        }
        if ( ost == 1 ) { 
             if ( U[k-1] == 0 ) {
                  Sum[k-1] = 1;
                  ost = 0;
                  continue;
             } else {  
                 Sum[k-1] = 0; 
                 continue;
             }
        } 
        Sum[k-1] = U[k-1];
    }    
    if ( ost == 1 ) {
         Sum = ( int * ) realloc ( Sum, ( ++k )*sizeof( int ) );
         Sum[k-1] = 1;
         if ( k > 1000000000 ) {
             repletion exp;
             throw exp;        
        }
    } 
    Sumlen = k;
    return Sum;
}
//------------------------------------------------------------------------------
int *multiplybin( int *U, int *W, int sizeu, int sizew, int &Proizvlen ) {
      
    int *Proizv = NULL;
    int i, k, j;
    int l;
    int len = sizeu;
    int q = 0;
    int *Proizv1 = NULL;
    int r = sizeu;
    
    Proizv = ( int * ) malloc ( sizeu*sizeof( int ) );
    Proizv1 = ( int * ) malloc ( sizeu*sizeof( int ) );
    Proizv1 = U;
    
    for( i = 0; i < sizeu; ++i) Proizv[i] = 0;
    for ( i = 0; i < sizew; i++ ) {
        if(W[i]==0) continue;
        else if ( W[i] == 1 )   { 
            turnstring ( Proizv1, r );
            l = r + ( i - q );
            q = i;
            k = r;
            while( k != l ) {        
                Proizv1 = ( int * ) realloc ( Proizv1, ( ++k )*sizeof( int ) );
                Proizv1[k-1] = 0;
            }
            r = l;
            turnstring( Proizv1, l );
            Proizv = plusbin( Proizv1, Proizv, l, len, len);
        }
    }
    Proizvlen = len;
    return Proizv;
}
//------------------------------------------------------------------------------
int *minusbin ( int *U, int *W, int sizeu, int sizew, int &Minuslen ) {

    int i = 0;
    int k;
    int j;
    int *Minus = NULL;
    
    while ( i != sizew ) {
        Minus = ( int * ) realloc ( Minus, ( ++i )*sizeof( int ) );
        if ( ( ( U[i-1] == 0 ) && ( W[i-1] == 0 ) ) || ( ( U[i-1] == 1 ) && ( W[i-1] == 1 ) ) ) 
            Minus[i-1] = 0;
        else if ( ( U[i-1] == 1 ) && ( W[i-1] == 0 ) ) {
            Minus[i-1] = 1;
        } else if ( ( U[i-1] == 0 ) && ( W[i-1] == 1 ) ) {
             k = i;
             while ( U[k] != 1 ) {
                 k++;
             }
             U[k] = 0;
             Minus[i-1] = 1;
             for( j = i; j < k; j++ ) U[j] = 1;
        }
    }
    i = sizew;
    while( i != sizeu ) {
        Minus = ( int * ) realloc ( Minus, ( ++i )*sizeof( int ) );
        Minus[i-1] = U[i-1];
    }    
    Minuslen = i;
    return Minus;
}
//------------------------------------------------------------------------------
int *divisionbin ( int *U, int *W, int sizeu, int sizew, int &Divlen ) {
    
    int i, j, k;
    int *Div = NULL;
    int *Div1 = NULL;
    int *U1 = NULL;
    bool com;
    int f = 0;
    int sized = 0, sizeu1 = sizew, sized1;

    U1 = ( int * ) malloc ( sizew * sizeof( int ) );
    turnstring ( U, sizeu );
    turnstring ( W, sizew );
    for ( i = 0; i < sizew; i++ ) U1[i] = U[i];
    com = Compare ( U1, W, sizew );
    if ( com == true ) {
       Div = ( int * ) realloc ( Div, ( ++sized )*sizeof( int ) );
       Div[sized-1] = 1;
    } else {   
        sizeu1 = sizeu1 + 1;
        U1 = ( int * ) realloc ( U1, sizeu1*sizeof( int ) );
        U1[sizeu1-1] = U[sizeu1-1]; 
        Div = ( int * ) realloc ( Div, ( ++sized )*sizeof( int ) );
        Div[sized-1] = 1;
        com = true;
    }
    turnstring( U1, sizeu1 ); 
    turnstring( W, sizew );
    Div1 = minusbin ( U1, W, sizeu1, sizew, sized1 );
    turnstring( Div1, sized1 );   
    turnstring( U1, sizeu1 ); 
    turnstring( W, sizew );
    j = sizeu1;
    while ( j != sizeu ) {
        if ( com == true ) {        
            free( U1 );
            U1 = NULL;
            i = 0;
            while ( Div1[i] == 0 ) i++;
            sizeu1 = sized1 - i;
            U1 = ( int * ) malloc ( sizeu1*sizeof( int ) );
            for ( k = 0; k < sizeu1; k++ ) U1[k] = Div1[k+i]; 
            com = false;   
            free ( Div1 );
            f = 0;
        }
        U1 = ( int * ) realloc ( U1, ( ++sizeu1 )*sizeof( int ) );
        U1[sizeu1-1] = U[j];    
        if ( U1[sizeu1-1] == 1 ) f = 1;
        if ( ( U1[sizeu1-1] == 0 ) && ( f == 0 ) && ( sizeu1 == 1 ) ) {
            Div = ( int * ) realloc ( Div, ( ++sized )*sizeof( int ) );
            Div[sized-1] = 0; 
            free ( U1 );
            U1 = NULL;
            sizeu1 = 0;
        }
        if ( ( sizeu1 < sizew ) && ( sizeu1 != 0 ) ) {
            Div = ( int * ) realloc ( Div, ( ++sized )*sizeof( int ) );
            Div[sized-1] = 0;    
        } else if ( sizeu1 == sizew ) {
            com = Compare( U1, W, sizew );
            if ( com == true ) {
                Div = ( int * ) realloc ( Div, ( ++sized )*sizeof( int ) );
                Div[sized-1] = 1;      
                turnstring( U1, sizeu1 ); 
                turnstring( W, sizew );
                Div1 = minusbin ( U1, W, sizeu1, sizew, sized1 );
                turnstring( Div1, sized1 );
                turnstring( U1, sizeu1 ); 
                turnstring( W, sizew );
            } else {
                Div = ( int * ) realloc ( Div, ( ++sized )*sizeof( int ) );
                Div[sized-1] = 0;   
            }
        } else if ( sizeu1 > sizew ) {
                Div = ( int * ) realloc ( Div, ( ++sized )*sizeof( int ) );
                Div[sized-1] = 1;      
                turnstring( U1, sizeu1 ); 
                turnstring( W, sizew );
                Div1 = minusbin ( U1, W, sizeu1, sizew, sized1 );
                turnstring( Div1, sized1 );
                turnstring( U1, sizeu1 ); 
                turnstring( W, sizew );
                com = true;
            }
        j++;     
    }
    turnstring ( Div, sized );
    Divlen = sized;
    return Div;
}
//------------------------------------------------------------------------------
int *BinToDec ( int *Sbin, int Slen, int *Sdec, int &Shlen ) {
     
    int i, j = 0, k, g, temp;
    Sdec = NULL;
    Sdec = ( int * ) malloc ( 1*sizeof( int ) );
    Sdec[0] = 0;
    int len = 1;
    int ost = 0;
    
    for ( i = Slen - 1; i > -1; --i  ) {
         for ( k = 0; k < len; ++k ) {
             Sdec[k] = Sdec[k] * 2 + ost;
             temp = Sdec[k];
             g = numbers( temp );
             if ( g > 4 ) {
                  ost = Sdec[k] / 10000;
                  Sdec[k] = Sdec[k] % 10000;
             } else ost = 0;
         }
         if ( ost != 0 ) {
             len = len + 1;
             Sdec = ( int * ) realloc ( Sdec, len*sizeof( int ) );
             Sdec[len-1] = ost;   
             ost = 0;      
         }    
         Sdec[0] = Sdec[0] + Sbin[i];
    }
    Shlen = len;  
    return Sdec;  
}
//------------------------------------------------------------------------------
int *DecToBin ( int razmer, int *X, int *Binary, int &s1 ) {
     
    int k = 0 i = 0;
    int a, b;
    Binary = NULL;
    a = X[razmer-1];
    int r = 1;
    
    while ( r == 1 ) {
        b = a % 2;
        Binary = ( int * ) realloc ( Binary, ( ++i )*sizeof( int ) ); 
        if ( i > 1000000000 ) {
            repletion exp;
            throw exp;        
            }
        Binary[i-1] = b;
        for ( k = 0; k < razmer; k++ ) {
            if ( ( ( X[k] % 2 ) == 1 ) && ( k != ( razmer - 1 ) ) ) X[k+1] = X[k+1] + 10000;
            X[k] = X[k] / 2;
            if ( k == ( razmer - 1 ) ) { 
                 a = X[k]; 
                 if ( a == 0 ) r = 0; 
                 else r = 1; 
            }
        }
    }
    s1 = i;
    return Binary;
}
//------------------------------------------------------------------------------
Long::Long () { 
        
        int i = 0;
        m = NULL;
        bin = NULL;
        m = ( int* ) malloc ( 1*sizeof( int ) );
        bin = ( int* ) malloc ( 1*sizeof( int ) );
        m[0] = 0;
        len = 1;
        bin[0] = 0;
        binlen = 1;
        flg = 1;
    }
//------------------------------------------------------------------------------
Long::Long ( string st ) {
         
        int s1 = st.length();
        char * s = new char[st.size()];
        strcpy( s, st.c_str() );
        
        char z;
        int i = 0;
        int size;
        int s2 = s1 / 4;
        int *temp = NULL;
    
        temp = ( int * ) malloc ( s2*sizeof( int ) );
        z = Test(s, '-');
        if ( z == true ) { 
            strcpy(s, s+1); 
            flg = 0;
        } else flg = 1;  
        if ( s1 > 4 ) {
            int r = s1 - s2 * 4;
            char temp_p[r];
            strncpy( temp_p, s, r );
            temp[0] = atoi( temp_p );
            strcpy( s, s + r );
            for ( i = 1; i < s2+1; i++ ) {
                char temp_s[4];
                strncpy( temp_s, s, 4 );
                temp[i] = atoi( temp_s );
                strcpy( s, s + 4 );
            }   
            size = s2 + 1;
        } else {
            temp[0] = atoi( s );
            size = 1;    
        }
        i = 0; 
        m = NULL;
        while ( i != size ) {
            m = ( int * ) realloc (m, ( ++i )*sizeof( int ) );
            m[i-1] = temp[i-1];
        }
        len = size;
        bin = DecToBin (size, temp, bin, binlen);    
    }
//------------------------------------------------------------------------------
    Long& Long::operator= ( const Long& A ) {
	
	    int i;
	
        if ( this == &A ) return *this;
        delete [] m;
	    delete [] bin;
        m = new int [len = A.len];
	    bin = new int [binlen = A.binlen];
        flg = A.flg;
	    for ( i = 0; i < len; i++ ) {
		    m[i] = A.m[i];
	    }
	    for ( i = 0; i < binlen; i++ ) {   
            bin[i] = A.bin[i];
        }
        return *this;
    }
//------------------------------------------------------------------------------                     
    Long operator+ ( const Long &A, const Long &B ) {
        
        int i;
        Long C;
        int f = 1;

        if ( A.flg == B.flg ) {           
            C.flg = A.flg;
            if ( A.flg == 1 ) {
                if ( A >= B )
                    C.bin = plusbin ( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
                else 
                    C.bin = plusbin ( B.bin, A.bin, B.binlen, A.binlen, C.binlen );
            } else if ( A.flg == 0 ) {
                if ( A >= B )
                    C.bin = plusbin ( B.bin, A.bin, B.binlen, A.binlen, C.binlen );
                else 
                    C.bin = plusbin ( A.bin, B.bin, A.binlen, B.binlen, C.binlen );   
            }
        } else if ( ( A.flg == 1 ) && ( B.flg == 0 ) ) {  
            if ( A.len > B.len ) {
                C.flg = 1;
                C.bin = minusbin ( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
            } else if ( A.len == B.len ) {    
                for ( i = A.len - 1; i > -1; --i ) {
                    if ( B.m[i] > A.m[i] ) {        
                        f = 0; 
                        break; 
                    }  
                } if ( f == 1 ) {   
                    C.flg = 1;
                    C.bin = minusbin ( A.bin, B.bin, A.binlen, B.binlen, C.binlen ); 
                } else {  
                    C.flg = 0;
                    C.bin = minusbin ( B.bin, A.bin, B.binlen, A.binlen, C.binlen );
                }
            } else {
                C.flg = 0;
                C.bin = minusbin ( B.bin, A.bin, B.binlen, A.binlen, C.binlen );
            }
        } else if ( ( A.flg == 0 ) && ( B.flg == 1 ) ) {  
            if ( B.len > A.len ) {
                C.flg = 1;
                C.bin = minusbin ( B.bin, A.bin, B.binlen, A.binlen, C.binlen);
            } else if ( A.len == B.len ) {   
                for ( i = A.len - 1; i > -1; --i ) {
                    if ( A.m[i] > B.m[i] ) { 
                        f = 0; 
                        break; 
                    }  
                }
                if ( f == 1 ) { 
                    C.flg = 1;
                    C.bin = minusbin ( B.bin, A.bin, B.binlen, A.binlen, C.binlen); 
                } else {  
                    C.flg = 0;
                    C.bin = minusbin ( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
                }
            } else {   
                C.flg = 0;
                C.bin = minusbin ( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
            }
        }
        C.m = BinToDec ( C.bin, C.binlen, C.m, C.len );
        turnstring( C.m, C.len );
        return C;
    }
//------------------------------------------------------------------------------
     Long operator- ( const Long &A, const Long &B ) {
        
        Long C;

        if ( ( A.flg == 1 ) && ( B.flg == 1 ) ) {   
            if ( A > B )    
                C.bin = minusbin ( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
            else if ( A == B ) return C;
            else {
                 C.flg = 0;
                 C.bin = minusbin ( B.bin, A.bin, B.binlen, A.binlen, C.binlen );
            }
        } else  if ( ( A.flg == 0 ) && ( B.flg == 0 ) ) {      
            if ( A < B ) { 
                C.flg = 0;
                C.bin = minusbin ( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
           }
           else if ( A == B ) return C;
           else {
               C.flg = 1;
               C.bin = minusbin ( B.bin, A.bin, B.binlen, A.binlen, C.binlen );
           }
        } else if ( ( A.flg == 1 ) && ( B.flg == 0 ) ) {
            C.flg = 1;
            Long D;
            D = Modul ( B );
            if ( A >= D ) C.bin = plusbin ( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
            else C.bin = plusbin ( B.bin, A.bin, B.binlen, A.binlen, C.binlen );
        } else if ( ( A.flg == 0 ) && ( B.flg == 1 ) ) {
             C.flg = 0;
             Long D;
             D = Modul ( A );
             if ( D >= B ) {
                  C.bin = plusbin ( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
             } else C.bin = plusbin ( B.bin, A.bin, B.binlen, A.binlen, C.binlen );
        }
        C.m = BinToDec ( C.bin, C.binlen, C.m, C.len );
        turnstring( C.m, C.len );
        return C;
    } 
//------------------------------------------------------------------------------
    Long operator* ( const Long &A, const Long &B ) {
        
        Long C;
        int f = 1;
        int i;
        
        if ( A.flg == B.flg ) {      
            C.flg = 1;
            if ( A.flg == 1 ) {
                if ( A >= B ) C.bin = multiplybin( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
                else C.bin = multiplybin( B.bin, A.bin, B.binlen, A.binlen, C.binlen );
            } else {  
                if ( A >= B ) C.bin = multiplybin( B.bin, A.bin, B.binlen, A.binlen, C.binlen );
                else C.bin = multiplybin( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
              }
            } else {   
                C.flg = 0;
                if (  A.len > B.len ) C.bin = multiplybin( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
                else if ( A.len < B.len ) C.bin = multiplybin( B.bin, A.bin, B.binlen, A.binlen, C.binlen );
                else if ( A.len == B.len ) {
                    for ( i = A.len - 1; i > -1; --i ) {
                        if ( B.m[i] > A.m[i] ) { 
                            f = 0; 
                            break; 
                        }  
                    }
                    if ( f == 1 ) C.bin = multiplybin( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
                    else C.bin = multiplybin( B.bin, A.bin, B.binlen, A.binlen, C.binlen ); 
                }
            }
            C.m = BinToDec ( C.bin, C.binlen, C.m, C.len );
            turnstring( C.m, C.len );       
            return C;
    }
//------------------------------------------------------------------------------
    Long operator/ ( const Long &A, const Long &B ) {
        
        Long C;
        
        if ( B == C ) {
            zero z;
            throw z;
        }
            if ( A.flg == B.flg ) {        
                 C.flg = 1;
                 if ( A.flg == 1 ) {
                     if ( A < B ) return C;
                     else if ( A == B ) { 
                         C.bin[0] = 1;
                         C.m[0] = 1;
                         return C;
                     } else C.bin = divisionbin( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
                 }
                 if ( A.flg == 0 ) {
                     if ( A > B ) return C;
                     else if ( A == B ) { 
                         C.bin[0] = 1;
                         C.m[0] = 1;
                         return C;
                     } else C.bin = divisionbin( A.bin, B.bin, A.binlen, B.binlen, C.binlen );
                 }
            } else {  
                C.flg = 0;
                Long D;
                if ( A.flg == 0 ) {
                    D = Modul ( A );
                    if ( D < B ) return C;
                    else if ( D == B ) {   
                         C.bin[0] = 1;
                         C.m[0] = 1;
                         return C;
                    } else C.bin = divisionbin( D.bin, B.bin, D.binlen, B.binlen, C.binlen );
                }
                if ( B.flg == 0 ) {
                    D = Modul ( B );
                    if ( A < D ) return C;
                    else if ( A == D ) { 
                         C.bin[0] = 1;
                         C.m[0] = 1;
                         return C;
                    } else C.bin = divisionbin( A.bin, D.bin, A.binlen, D.binlen, C.binlen );
                }
            }
            C.m = BinToDec ( C.bin, C.binlen, C.m, C.len );
            turnstring( C.m, C.len );    
            return C;
    }
//------------------------------------------------------------------------------
     Long operator% ( const Long &A, const Long &B ) {
        
        Long C;
        Long D;

        D = A / B;
        D = B * D;
        C = A - D;      
        C.flg = 1;       
        return C;
     }
//------------------------------------------------------------------------------
     Long operator+= ( Long &A, const Long B ) {
       
         A = A + B;
         return A;   
     }
//------------------------------------------------------------------------------
     Long operator-= ( Long &A, const Long B ) {
       
         A = A - B;
         return A;   
    }
//------------------------------------------------------------------------------
     Long operator*= ( Long &A, const Long B ) {
       
         A = A * B;
         return A;   
     }
//------------------------------------------------------------------------------
     Long operator/= ( Long &A, const Long B ) {
       
         A = A / B;
         return A;   
     }
//------------------------------------------------------------------------------
     bool operator > ( const Long &A, const Long &B ) {
        
        int i;
        if ( ( A.flg == 1 ) && ( B.flg == 0 ) )   return true;   
        else if ( ( A.flg == 0 ) && ( B.flg == 1 ) )   return false;
        else if ( ( A.flg == 1 ) && ( B.flg == 1 ) ) {
            if ( A.len > B.len )   return true;
            else if ( A.len < B.len )   return false;
            else if ( A.len == B.len ) {
                int i;
                for ( i = 0; i < A.len; i++ ) {
                    if ( A.m[i] == B.m[i] ) continue;
                    else if ( A.m[i] < B.m[i] ) return false;
                    else if ( A.m[i] > B.m[i] ) return true;
                }       
                return false;
            } 
        }
        else if ( ( A.flg == 0 ) && ( B.flg == 0 ) ) {
            if ( A.len > B.len )   return false;
            else if ( A.len < B.len )   return true;
            else if ( A.len == B.len ) {
                for ( i = 0; i < A.len; i++ ) {  
                    if ( A.m[i] == B.m[i] ) continue;
                    else if ( A.m[i] < B.m[i] ) return true;
                    else if ( A.m[i] > B.m[i] ) return false;
                }       
                return false;
            } 
        }
    }
//------------------------------------------------------------------------------
     bool operator < ( const Long &A, const Long &B ) {      
    
        return B > A;
    }
//------------------------------------------------------------------------------
     bool operator== ( const Long &A, const Long &B ) {
        
        int i;
        if ( A.flg != B.flg ) return false;
        else {
             if ( A.len != B.len ) return false;
             else {
                 for ( i = 0; i < A.len; i++ ) {
                     if ( A.m[i] != B. m[i] ) return false;   
                 }
                 return true;
             }  
        }
    }
//------------------------------------------------------------------------------
     bool operator!= ( const Long &A, const Long &B ) {
        
        return !( A == B );
    }
//------------------------------------------------------------------------------
     bool operator>= ( const Long &A, const Long &B ) {
           
           if ( A < B ) return false; 
           else return true;
    }
//------------------------------------------------------------------------------
     bool operator<= ( const Long &A, const Long &B ) {
       
        if ( A > B ) return false;
        else return true;
    }  
//------------------------------------------------------------------------------      
     istream &operator >> ( istream &in, Long &C ) { 

        string str;
        getline(cin,str);  
        Long D(str);
        C = D;
        return in;     
    }
//------------------------------------------------------------------------------
     ostream &operator << ( ostream &out, const Long &C ){
        
        int l = C.len;
        int i = 0;
        int r, g;
        int k;
        int h;
        
        if ( C.flg == 0 ) out << "-";
        while ( i != l ) {  
            h = C.m[i];
            g = numbers( h );
            if ( ( g < 4 ) && ( i != 0 ) ) {
                r = 4 - g;
                for ( k = 0; k < r; k++ )
                    out << "0";  
            }
            out << C.m[i];
            i++;
        }
    return out;
    }
//------------------------------------------------------------------------------
Long Modul ( Long A ) {

	A.flg = 1;
	return A;
}

