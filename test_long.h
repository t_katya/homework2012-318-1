#ifndef TEST_LONG_H
#define TEST_LONG_H

#include"Long.h"
#include <cppunit/extensions/HelperMacros.h>

using namespace CppUnit;

class Test_Long : public TestFixture {
      
    CPPUNIT_TEST_SUITE(Test_Long);
	CPPUNIT_TEST(ADD);
	CPPUNIT_TEST(SUB);
	CPPUNIT_TEST(MUL);
	CPPUNIT_TEST(DIV);
	CPPUNIT_TEST(LESS);
	CPPUNIT_TEST(MORE);
	CPPUNIT_TEST(LESSEQUAL);
	CPPUNIT_TEST(MOREEQUAL);
	CPPUNIT_TEST(EQUAL);
	CPPUNIT_TEST(NOTEQUAL);
	CPPUNIT_TEST_SUITE_END();

  public:
      void setUp();
	  void tearDown();	
	  void ADD();
	  void SUB();
	  void MUL();
	  void DIV();
	  void LESS();
	  void MORE();
	  void LESSEQUAL();
	  void MOREEQUAL();
	  void EQUAL();
	  void NOTEQUAL();
 
  private:
	  Long *A, *B, *C;

};

#endif
