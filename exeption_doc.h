﻿#ifndef EXCEPTION_ARR_H
#define EXCEPTION_ARR_H
#include<iostream>

/**
  * Данный класс реализует общее исключение
  * функция <tt>exep()</tt> является виртуальной
  * @author Taratuta Yekaterina
 */

class exception_long {
      
  public:
         
      virtual void exep();
};

/**
 * Класс zero. Исключение: деление на ноль. 
 * Функция <tt>exep()</tt> печатает сообщение о том, что произошло деление на ноль.
 * Класс является наследником exception_long.
 * Исключение вызывается в catch - обработчике
*/

class  zero: public exception_long {
       
  public:
         
    void exep();
};
/**
 * ласс repletion. Исключение: переполнение
 * Функция <tt>exep()</tt> печатает сообщение о том, что произошло переполнение.
 * Класс является наследником exception_long.
 * Исключение вызывается в catch - обработчике
*/
class  repletion: public exception_long {
       
  public:
         
      void exep();
};

#endif 
